# NorthPoint Installer 3.0.4.2
###### Copyright (c) Charlie Howard 2016-2018 All rights reserved.

When run you will get a form that allows to select what programs you wish to install.
Can install the Computer Repair Centre OEM information, 7-zip, Google Chrome, iTunes, Kaspersky Internet Security 2018 (uninstalls Kaspersky Secure Connection), LibreOffice, Mozilla Firefox, Mozilla Thunderbird, Skype, TeamViewer, uBlock Origin & VLC Media Player.
If run on Windows 8 and 8.1 it also changes the default Explorer page to "This PC".
If run on Windows 10 it also disables Windows 10 hibernation mode, Wi-Fi sense, and changes the default Explorer page to "This PC".
Changes wallpaper to a selection of HD landscape images.